<?php

class Activity extends Eloquent {
	protected $guarded = array();

	public static $rules = array();

	public function records()
	{
		 return $this->hasMany('Record');
	}

	public function users()
	{
		 return $this->belongsToMany('User');
	}

	public function groups()
	{
		 return $this->belongsToMany('Group');
	}
}
