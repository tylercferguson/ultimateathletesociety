<?php

class Tournament extends Eloquent {
	protected $guarded = array();

	public static $rules = array();

	public function records()
	{
		 return $this->hasMany('Record');
	}

	public function users()
	{
		 return $this->belongsToMany('User')->withPivot('registered', 'transaction_id', 'place');
	}

	public function groups()
	{
		 return $this->belongsToMany('Group')->withPivot('place');
	}
}
