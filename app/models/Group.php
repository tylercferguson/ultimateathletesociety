<?php

use Cartalyst\Sentry\Groups\Eloquent\Group as SentryGroupModel;

class Group extends SentryGroupModel {
	public function tournaments()
	{
		 return $this->belongsToMany('Tournament')->withPivot('place');
	}
}
