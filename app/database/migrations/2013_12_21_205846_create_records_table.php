<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('records', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('tournament_id')->nullable();
			$table->integer('stat_id');
			$table->float('amount');
			$table->integer('subevent_id')->nullable();
			$table->integer('activity_id')->nullable();
			$table->integer('duel_id')->nullable();
			$table->integer('challenge_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('group_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('records');
	}

}
