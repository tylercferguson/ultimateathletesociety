<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotDuelGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('duel_group', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('duel_id')->unsigned()->index();
			$table->integer('group_id')->unsigned()->index();
			$table->foreign('duel_id')->references('id')->on('duels')->onDelete('cascade');
			$table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
			$table->integer('win')->default(0);
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('duel_group');
	}

}
