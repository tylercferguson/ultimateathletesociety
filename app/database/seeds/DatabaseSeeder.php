<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('PostsSeeder');
		$this->call('CommentsSeeder');
		$this->call('AreasTableSeeder');
		$this->call('LocationsTableSeeder');
		$this->call('AssetsTableSeeder');
		$this->call('EventsTableSeeder');
		$this->call('BusinessassociatesTableSeeder');
		$this->call('CataloguesTableSeeder');
		$this->call('ClassificationsTableSeeder');
		$this->call('ConsentsTableSeeder');
		$this->call('ConsultationsTableSeeder');
		$this->call('DisputesTableSeeder');
		$this->call('ContractsTableSeeder');
		$this->call('SubeventsTableSeeder');
		$this->call('StatsTableSeeder');
		$this->call('RecordsTableSeeder');
		$this->call('ChallengesTableSeeder');
		$this->call('DuelsTableSeeder');
		$this->call('ActivitiesTableSeeder');
		$this->call('TransactionsTableSeeder');
		$this->call('TournamentsTableSeeder');
		$this->call('TeamsTableSeeder');
	}

}
