@extends('layouts/inside')

@section('content')

<ul class="breadcrumb">
	<li><a href="{{route('home')}}" class="glyphicons home"><i></i> Ultimate Athlete Society</a></li>
	<li class="divider"></li>
	
</ul>

<div class="row row-merge">
	<div class="col-md-6">
	
	<!-- Inner -->
	<div class="innerAll">
	
		<!-- Row -->
		<div class="row">
			<div class="col-md-4">
			
				<!-- Stats Widget -->
				<span class="txt col-md-12" style="text-align:center;font-weight:bold;">Total Points</span>
				<a href="" class="widget-stats widget-stats-2 primary">

					<span class="sparkline"><i class="icon-star" style="font-size:50px;"></i></span>

					<span class="txt" style="padding-top:0px;"><span class="count">2,566</span></span>
					<div class="clearfix"></div>
				</a>
				<!-- // Stats Widget END -->
				
			</div>
			<div class="col-md-4">
			
				<!-- Stats Widget -->
				<span class="txt col-md-12" style="text-align:center;font-weight:bold;">Individual Rank</span>
				<a href="" class="widget-stats widget-stats-2 primary">

					<span class="sparkline"><i class="icon-bar-chart" style="font-size:50px;"></i></span>

					<span class="txt" style="padding-top:0px;"><span class="count">2,566</span> of 2,566</span>
					<div class="clearfix"></div>
				</a>
				<!-- // Stats Widget END -->
				
			</div>
			<div class="col-md-4">
			
				<!-- Stats Widget -->
				<span class="txt col-md-12" style="text-align:center; font-weight:bold;">Activity Points</span>
				<a href="" class="widget-stats widget-stats-2 primary">

					<span class="sparkline"><div class="glyphicons blacksmith"><i></i></div></span>

					<span class="txt" style="padding-top:0px;"><span class="count">2,566</span></span>
					<div class="clearfix"></div>
				</a>
				<!-- // Stats Widget END -->
				
			</div>
		</div>
		<!-- // Row END -->
	
	</div>
	<!-- // Inner END -->
	
	</div>
	<div class="col-md-6">
	
	<!-- Inner -->
	<div class="innerAll">
		
		<!-- Row -->
		<div class="row">
			<div class="col-md-4">
			
				<!-- Stats Widget -->
				<span class="txt col-md-12" style="text-align:center; font-weight:bold;">Downtime Losses </span>
				<a href="" class="widget-stats widget-stats-2 primary">

					<span class="sparkline"><div class="glyphicons warning_sign"><i></i></div></span>

					<span class="txt" style="padding-top:0px;"><span class="count">2,566</span> BOE</span>
					<div class="clearfix"></div>
				</a>
				<!-- // Stats Widget END -->
				
			</div>
			<div class="col-md-4">
			
				<!-- Stats Widget -->
				<span class="txt col-md-12" style="text-align:center; font-weight:bold;">Issues Resolved</span>
				<a href="" class="widget-stats widget-stats-2 primary">

					<span class="sparkline"><div class="glyphicons wrench"><i></i></div></span>

					<span class="txt" style="padding-top:0px;"><span class="count">3%</span> (3 total)</span>
					<div class="clearfix"></div>
				</a>
				<!-- // Stats Widget END -->
				
			</div>
			<div class="col-md-4">
			
				<!-- Stats Widget -->
				<span class="txt col-md-12" style="text-align:center;font-weight:bold;">Man Hours</span>
				<a href="" class="widget-stats widget-stats-2 warning">

					<span class="sparkline"><div class="glyphicons user"><i></i></div></span>

					<span class="txt" style="padding-top:0px;"><span class="count">2,566</span> Hours</span>
					<div class="clearfix"></div>
				</a>
				<!-- // Stats Widget END -->
				
			</div>
			
		</div>
		<!-- // Row END -->
	
	</div>
	<!-- // Inner END -->
	
	</div>
</div>

<div class="innerLR">
	<div class="row">
		<div class="col-md-8">
			<div class="widget" data-toggle="collapse-widget">
				<div class="widget-head">
					<h4 class="heading glyphicons cardio"><i></i>Weekly Production</h4>
				</div>
				<div class="widget-body collapse in">
					<div id="weekly_production" style="height: 250px;"></div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="widget" data-toggle="collapse-widget">
				<div class="widget-head">
					<h4 class="heading glyphicons pie_chart"><i></i>Issue Causes</h4>
				</div>
				<div class="widget-body collapse in">
					<div id="chart_issues" style="height: 250px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<!--  Flot Charts Plugin -->
<script src="{{asset('admin-ui/common/theme/scripts/plugins/charts/flot/jquery.flot.js')}}"></script>
<script src="{{asset('admin-ui/common/theme/scripts/plugins/charts/flot/jquery.flot.pie.js')}}"></script>
<script src="{{asset('admin-ui/common/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js')}}"></script>
<script src="{{asset('admin-ui/common/theme/scripts/plugins/charts/flot/jquery.flot.selection.js')}}"></script>
<script src="{{asset('admin-ui/common/theme/scripts/plugins/charts/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('admin-ui/common/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js')}}"></script>
<script>

var charts = 
{
	// utility class
	utility:
	{
		chartColors: [ "#34495E", "#F1C40F", "#2980B9", "#C0392B", "#2C3E50", "#F39C12" ],
		chartBackgroundColors: ["#fff", "#fff"],

		applyStyle: function(that)
		{
			//that.options.colors = charts.utility.chartColors;
			that.options.grid.backgroundColor = { colors: charts.utility.chartBackgroundColors };
			that.options.grid.borderColor = charts.utility.chartColors[0];
			that.options.grid.color = charts.utility.chartColors[0];
		},
		
		// generate random number for charts
		randNum: function()
		{
			return (Math.floor( Math.random()* (1+40-20) ) ) + 20;
		}
	},
	chart_pie:
	{
		// chart data
		data: [
		    { label: "Equipment",  data: 38 },
		    { label: "Personnel",  data: 23 },
		    { label: "Electric",  data: 15 },
		    { label: "Contractor",  data: 9 },
		    
		],

		// will hold the chart object
		plot: null,

		// chart options
		options: 
		{
			series: {
				pie: { 
					show: true,
					highlight: {
						opacity: 0.1
					},
					radius: 1,
					stroke: {
						color: '#fff',
						width: 2
					},
					startAngle: 2,
				    combine: {
	                    color: '#353535',
	                    threshold: 0.05
	                },
	                label: {
	                    show: true,
	                    radius: 1,
	                    formatter: function(label, series){
	                        return '<div class="label label-inverse">'+label+'&nbsp;'+Math.round(series.percent)+'%</div>';
	                    }
	                }
				},
				grow: {	active: false}
			},
			colors: [],
			legend:{show:false},
			grid: {
	            hoverable: true,
	            clickable: true,
	            backgroundColor : { }
	        },
	        tooltip: true,
			tooltipOpts: {
				content: "%s : %y.1"+"%",
				shifts: {
					x: -30,
					y: -50
				},
				defaultTheme: false
			}
		},
		
		// initialize
		init: function()
		{
			// apply styling
			charts.utility.applyStyle(this);
			
			this.plot = $.plot($("#chart_issues"), this.data, this.options);
		}
	},

	// lines chart with fill & without points
	weekly_production: 
	{
		// chart data
		data: 
		{
			d1: [],
			d2: []
		},

		// will hold the chart object
		plot: null,

		// chart options
		options: 
		{
			grid: {
				show: true,
			    aboveData: true,
			    color: "#3f3f3f",
			    labelMargin: 5,
			    axisMargin: 0, 
			    borderWidth: 0,
			    borderColor:null,
			    minBorderMargin: 5 ,
			    clickable: true, 
			    hoverable: true,
			    autoHighlight: true,
			    mouseActiveRadius: 20,
			    backgroundColor : { }
			},
	        series: {
	        	grow: {active:false},
	            lines: {
            		show: true,
            		fill: 0.02,
            		lineWidth: 2,
            		steps: false
            	},
	            points: {show:false}
	        },
	        legend: { position: "nw" },
	        yaxis: { min: 0 },
	        xaxis: {ticks:11, tickDecimals: 0},
	        colors: ["#2ECC71", "#E74C3C"],
	        shadowSize:1,
	        tooltip: true,
			tooltipOpts: {
				content: "%s : %y.0",
				shifts: {
					x: -30,
					y: -50
				},
				defaultTheme: false
			}
		},

		// initialize
		init: function()
		{
			// apply styling
			charts.utility.applyStyle(this);
			
			// generate some data
			this.data.d1 = [[1, 3+charts.utility.randNum()], [2, 6+charts.utility.randNum()], [3, 9+charts.utility.randNum()], [4, 12+charts.utility.randNum()],[5, 15+charts.utility.randNum()],[6, 18+charts.utility.randNum()],[7, 21+charts.utility.randNum()],[8, 15+charts.utility.randNum()],[9, 18+charts.utility.randNum()],[10, 21+charts.utility.randNum()],[11, 24+charts.utility.randNum()],[12, 27+charts.utility.randNum()],[13, 30+charts.utility.randNum()],[14, 33+charts.utility.randNum()],[15, 24+charts.utility.randNum()],[16, 27+charts.utility.randNum()],[17, 30+charts.utility.randNum()],[18, 33+charts.utility.randNum()],[19, 36+charts.utility.randNum()],[20, 39+charts.utility.randNum()],[21, 42+charts.utility.randNum()],[22, 45+charts.utility.randNum()],[23, 36+charts.utility.randNum()],[24, 39+charts.utility.randNum()],[25, 42+charts.utility.randNum()],[26, 45+charts.utility.randNum()],[27,38+charts.utility.randNum()],[28, 51+charts.utility.randNum()],[29, 55+charts.utility.randNum()], [30, 60+charts.utility.randNum()]];
			this.data.d2 = [[1, charts.utility.randNum()-5], [2, charts.utility.randNum()-4], [3, charts.utility.randNum()-4], [4, charts.utility.randNum()],[5, 4+charts.utility.randNum()],[6, 4+charts.utility.randNum()],[7, 5+charts.utility.randNum()],[8, 5+charts.utility.randNum()],[9, 6+charts.utility.randNum()],[10, 6+charts.utility.randNum()],[11, 6+charts.utility.randNum()],[12, 2+charts.utility.randNum()],[13, 3+charts.utility.randNum()],[14, 4+charts.utility.randNum()],[15, 4+charts.utility.randNum()],[16, 4+charts.utility.randNum()],[17, 5+charts.utility.randNum()],[18, 5+charts.utility.randNum()],[19, 2+charts.utility.randNum()],[20, 2+charts.utility.randNum()],[21, 3+charts.utility.randNum()],[22, 3+charts.utility.randNum()],[23, 3+charts.utility.randNum()],[24, 2+charts.utility.randNum()],[25, 4+charts.utility.randNum()],[26, 4+charts.utility.randNum()],[27,5+charts.utility.randNum()],[28, 2+charts.utility.randNum()],[29, 2+charts.utility.randNum()], [30, 3+charts.utility.randNum()]];
			
			// make chart
			this.plot = $.plot(
				'#weekly_production', 
				[{
         			label: "Actual Production", 
         			data: this.data.d1,
         			lines: {fillColor: "#2ECC71", fill: 0.02},
         			points: {fillColor: "#2ECC71", fill: 0.02}
         		}, 
         		{	
         			label: "Target Production", 
         			data: this.data.d2,
         			lines: {fillColor: "#E74C3C", fill: 0.002},
         			points: {fillColor: "#1ABC9C", fill: 0.002}
         		}], 
         		this.options);
		}
	},
}

$(document).ready(function(){
	charts.chart_pie.init();
	charts.weekly_production.init();
});
</script>
@stop