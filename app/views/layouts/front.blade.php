<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Energy Control Tower</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="{{asset('front-ui/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Loading Flat UI -->
    <link href="{{asset('front-ui/css/flat-ui.css')}}" rel="stylesheet">
    <link href="{{asset('front-ui/css/whhg.css')}}" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      @yield('content')
    </div>
    <!-- /.container -->


    <!-- Load JS here for greater good =============================-->
    <script src="{{asset('front-ui/js/jquery-1.8.3.min.js')}}"></script>
    <script src="{{asset('front-ui/js/jquery-ui-1.10.3.custom.min.js')}}"></script>
    <script src="{{asset('front-ui/js/jquery.ui.touch-punch.min.js')}}"></script>
    <script src="{{asset('front-ui/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('front-ui/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('front-ui/js/bootstrap-switch.js')}}"></script>
    <script src="{{asset('front-ui/js/flatui-checkbox.js')}}"></script>
    <script src="{{asset('front-ui/js/flatui-radio.js')}}"></script>
    <script src="{{asset('front-ui/js/jquery.tagsinput.js')}}"></script>
    <script src="{{asset('front-ui/js/jquery.placeholder.js')}}"></script>
  </body>
</html>
