<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <html class="ie gt-ie8"> <![endif]-->
<!--[if !IE]><!--><html><!-- <![endif]-->
<head>
	<title>Energy Control Tower</title>
	
	<!-- Meta -->
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	
	<!-- Bootstrap -->
	<link href="{{asset('admin-ui/common/bootstrap/css/bootstrap.css')}}" rel="stylesheet" />
	
	<!-- Glyphicons Font Icons -->
	<link href="{{asset('admin-ui/common/theme/fonts/glyphicons/css/glyphicons_social.css')}}" rel="stylesheet" />
	<link href="{{asset('admin-ui/common/theme/fonts/glyphicons/css/glyphicons_filetypes.css')}}" rel="stylesheet" />
	<link href="{{asset('admin-ui/common/theme/fonts/glyphicons/css/glyphicons_regular.css')}}" rel="stylesheet" />
	<link href="{{asset('admin-ui/common/theme/css/whhg.css')}}" rel="stylesheet" />

	<!-- Font Awsome Icons -->
	<link href="{{asset('admin-ui/common/theme/fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
	
	<!-- Uniform Pretty Checkboxes -->
	<link href="{{asset('admin-ui/common/theme/scripts/plugins/forms/pixelmatrix-uniform/css/uniform.default.css')}}" rel="stylesheet" />
	
	<!-- PrettyPhoto -->
    <link href="{{asset('admin-ui/common/theme/scripts/plugins/gallery/prettyphoto/css/prettyPhoto.css')}}" rel="stylesheet" />
	
	<!-- Main Theme Stylesheet :: CSS -->
	<link href="{{asset('admin-ui/common/theme/css/style-light.css')}}" rel="stylesheet" />
	
	
	<!-- LESS.js Library -->
	<script src="{{asset('admin-ui/common/theme/scripts/plugins/system/less.min.js')}}"></script>
</head>
<body class="">
	
		<!-- Main Container Fluid -->
	<div class="container fluid menu-left">
		
		<!-- Top navbar -->
		<div class="navbar main hidden-print">
		
			<!-- Brand -->
			<a href="{{URL::route('admin')}}" class="appbrand pull-left"><span>Control Tower</span></a>
			
						<!-- Menu Toggle Button -->
			<button type="button" class="btn btn-navbar">
				<span class="glyphicons show_lines"><i></i></span>
			</button>
			<!-- // Menu Toggle Button END -->
						
						
						
			<!-- Top Menu Right -->
			<ul class="topnav pull-right">
			
				
			
				<!-- Dropdown -->
				<li class="dropdown visible-abc">
					<a href="" data-toggle="dropdown" class="glyphicons cogwheel"><i></i>{{Sentry::getUser()->first_name}} {{Sentry::getUser()->last_name}} <span class="caret"></span></a>
					<ul class="dropdown-menu pull-right">
						
						
		                
		                <li><a href="{{ route('profile') }}">View Your Profile</a></li>
						
						<li><a href="{{ route('logout') }}">Logout</a></li>
		                
					</ul>
				</li>
				<!-- // Dropdown END -->
				
				
				
			</ul>
			<!-- // Top Menu Right END -->
			
						
		</div>
		<!-- Top navbar END -->
		
				<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
		
		<!-- Sidebar Menu -->
		<div id="menu" class="hidden-sm hidden-print">
		
			<!-- Scrollable menu wrapper with Maximum height -->
			<div class="slim-scroll" data-scroll-height="800px">
			
			
			<!-- Sidebar Mini Stats -->
			<div id="notif">
				<ul>
					
					<li><a href="" class="glyphicons log_book"><i></i> 3 </a></li>
					<li><a href="" class="glyphicons bell"><i></i> 2 </a></li>
					
				</ul>
			</div>
			<!-- // Sidebar Mini Stats END -->
			
			<!-- Regular Size Menu -->
			<ul>
			
								<!-- Blank page template menu example -->
				<!-- Menu Regular Item (add class active to LI for an active menu item) -->
				<li class="glyphicons dashboard"><a href="{{route('home')}}"><i></i><span>Dashboard</span></a></li>
				
				<!-- Areas -->
				<li class="hasSubmenu glyphicons globe_af">
					<a data-toggle="collapse" href="#submenu-1"><i></i><span>Operational Areas</span></a>
					<ul class="collapse" id="submenu-1">
						@foreach(Sentry::getUser()->areas as $area)
						<li><a href="{{action('AreasController@show', $area->id)}}"><span>{{$area->name}}</span></a></li>
						@endforeach
						<li><a href="{{action('AreasController@create')}}"><span>Add a New Area</span></a></li>	
					</ul>
					<span class="count">{{Sentry::getUser()->areas->count()}}</span>
				</li>
				<!-- // Areas END -->
				

				@if(Sentry::getUser()->hasAccess('admin'))
				<!-- Users -->
				<li class="hasSubmenu glyphicons user">
					<a data-toggle="collapse" href="#submenu-2"><i></i><span>Personnel</span></a>
					<ul class="collapse" id="submenu-2">
						<li><a href="{{route('users')}}"><span>Manage Personnel</span></a></li>
						<li><a href="{{route('create/user')}}"><span>Add a New User</span></a></li>	
					</ul>
					
				</li>

				<!-- Users -->
				<li class="hasSubmenu glyphicons group">
					<a data-toggle="collapse" href="#submenu-2"><i></i><span>Groups</span></a>
					<ul class="collapse" id="submenu-2">
						<li><a href="{{route('groups')}}"><span>Manage Groups</span></a></li>
						<li><a href="{{route('create/group')}}"><span>Add a New Group</span></a></li>	
					</ul>
					
				</li>
				<!-- // Users END -->
				@endif
								
								
			</ul>
			<div class="clearfix"></div>
			<div class="separator bottom"></div>
			<!-- // Regular Size Menu END -->
			
						
						
						
			</div>
			<!-- // Scrollable Menu wrapper with Maximum Height END -->
			
		</div>
		<!-- // Sidebar Menu END -->
				
		<!-- Content -->
		<div id="content">
	@yield('content')
		</div>
		<!-- // Content END -->
			
				</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
				
		
		
	</div>
	<!-- // Main Container Fluid END -->
	
	

	<!-- JQuery -->
	<script src="{{asset('admin-ui/common/theme/scripts/plugins/system/jquery.min.js')}}"></script>
	
	<!-- JQueryUI -->
	<script src="{{asset('admin-ui/common/theme/scripts/plugins/system/jquery-ui/js/jquery-ui-1.9.2.custom.min.js')}}"></script>
	
	<!-- JQueryUI Touch Punch -->
	<!-- small hack that enables the use of touch events on sites using the jQuery UI user interface library -->
	<script src="{{asset('admin-ui/common/theme/scripts/plugins/system/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>

	<!-- Modernizr -->
	<script src="{{asset('admin-ui/common/theme/scripts/plugins/system/modernizr.js')}}"></script>
	
	<!-- Bootstrap -->
	<script src="{{asset('admin-ui/common/bootstrap/js/bootstrap.min.js')}}"></script>
	
	<!-- SlimScroll Plugin -->
	<script src="{{asset('admin-ui/common/theme/scripts/plugins/other/jquery-slimScroll/jquery.slimscroll.min.js')}}"></script>
	
	<!-- Common Demo Script -->
	<script src="{{asset('admin-ui/common/theme/scripts/demo/common.js')}}"></script>
	
	<!-- Holder Plugin -->
	<script src="{{asset('admin-ui/common/theme/scripts/plugins/other/holder/holder.js')}}"></script>
	
	<!-- Uniform Forms Plugin -->
	<script src="{{asset('admin-ui/common/theme/scripts/plugins/forms/pixelmatrix-uniform/jquery.uniform.min.js')}}"></script>
	
	<!-- PrettyPhoto -->
	<script src="{{asset('admin-ui/common/theme/scripts/plugins/gallery/prettyphoto/js/jquery.prettyPhoto.js')}}"></script>
	
	<!-- Global -->
	<script>
	var basePath = "{{asset('admin-ui/common/')}}";
	</script>

	@yield('scripts')

	
	
</body>
</html>